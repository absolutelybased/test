import * as PIXI from 'pixi.js';
import { app } from './setup';

import back from './assets/back.png';
import stairBlue from './assets/new_stair_01.png';
import stairRed from './assets/new_stair_02.png';
import stairGreen from './assets/new_stair_03.png';
import austin from './assets/Austin.png';
import oldStair from './assets/old_stair.png';
import dec from './assets/dec_2.png';

export const decContainer = new PIXI.Container();

decContainer.x = app.screen.width / 2;
decContainer.y = app.screen.height / 2;

decContainer.pivot.x = decContainer.width / 2;
decContainer.pivot.y = decContainer.height / 2;

const backSprite = new PIXI.Sprite(PIXI.Texture.from(back));
decContainer.addChild(backSprite);
backSprite.anchor.set(.5);

const austineSprite = new PIXI.Sprite(PIXI.Texture.from(austin));
decContainer.addChild(austineSprite);
austineSprite.anchor.set(.5);

const decSprite = new PIXI.Sprite(PIXI.Texture.from(dec));
decContainer.addChild(decSprite);
decSprite.anchor.set(.5);

export const oldStairSprite = new PIXI.Sprite(PIXI.Texture.from(oldStair));
decContainer.addChild(oldStairSprite);
oldStairSprite.anchor.set(.5);
oldStairSprite.x = app.screen.width / 2.25;
oldStairSprite.y = app.screen.height / 5;

export const blueStairSprite = new PIXI.Sprite(PIXI.Texture.from(stairBlue));
decContainer.addChild(blueStairSprite);
blueStairSprite.anchor.set(.5);
blueStairSprite.x = app.screen.width / 2.8;
blueStairSprite.y = app.screen.height / 7;
blueStairSprite.alpha = 0;

export const redStairSprite = new PIXI.Sprite(PIXI.Texture.from(stairRed));
decContainer.addChild(redStairSprite);
redStairSprite.anchor.set(.5);
redStairSprite.x = app.screen.width / 2.8;
redStairSprite.y = app.screen.height / 7;
redStairSprite.alpha = 0;

export const greenStairSprite = new PIXI.Sprite(PIXI.Texture.from(stairGreen));
decContainer.addChild(greenStairSprite);
greenStairSprite.anchor.set(.5);
greenStairSprite.x = app.screen.width / 2.8;
greenStairSprite.y = app.screen.height / 7;
greenStairSprite.alpha = 0;
