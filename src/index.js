import { gsap } from 'gsap';
import { app } from './setup';
import { decContainer } from './decorations';
import { interfaceContainer, hammerSprite, logo, logoSprite } from './interface';
import { menuContainer } from './menu';
import { logofilter } from './logofilter';

app.stage.addChild(decContainer);
app.stage.addChild(interfaceContainer);
app.stage.addChild(menuContainer);
app.stage.sortableChildren = true;
interfaceContainer.zIndex = 2;
menuContainer.zIndex = 1;
decContainer.zIndex = 0;

const tl = gsap.timeline();
tl.fromTo(app.view, { alpha: 0 }, { alpha: 1, duration: 1.5, ease: 'sine.in' })
  .to(hammerSprite, { alpha: 1, duration: .5, ease: 'sine.in' });

logoSprite.filters = [logofilter];

app.ticker.add((delta) => {
  logofilter.update(delta);
});
