import * as PIXI from 'pixi.js';
import { gsap } from 'gsap';
import { app } from './setup';
import { menuContainer, setInteractive } from './menu';

import btn from './assets/btn.png';
import logo from './assets/logo.png';
import hammer from './assets/icon_hammer.png';
import final from './assets/final.png';

export const interfaceContainer = new PIXI.Container();

interfaceContainer.x = app.screen.width / 2;
interfaceContainer.y = app.screen.height / 2;

interfaceContainer.pivot.x = interfaceContainer.width / 2;
interfaceContainer.pivot.y = interfaceContainer.height / 2;

export const logoSprite = new PIXI.Sprite(PIXI.Texture.from(logo));
interfaceContainer.addChild(logoSprite);
logoSprite.anchor.set(0);
logoSprite.x = -app.screen.width / 2 + 5;
logoSprite.y = -app.screen.height / 2 + 5;
logoSprite.scale.set(.75, .75);

const btnSprite = new PIXI.Sprite(PIXI.Texture.from(btn));
interfaceContainer.addChild(btnSprite);
btnSprite.anchor.set(.5);
btnSprite.y = app.screen.height / 3;
btnSprite.scale.set(.75, .75);
const temp = { x: .75 };
gsap.to(temp, { x: .6, yoyo: 1, repeat: -1, duration: .5,
   onUpdate: () => { btnSprite.scale.set(temp.x, temp.x) } })

export const hammerSprite = new PIXI.Sprite(PIXI.Texture.from(hammer));
interfaceContainer.addChild(hammerSprite);
hammerSprite.anchor.set(.5);
hammerSprite.alpha = 0;
hammerSprite.x = app.screen.width / 3;
hammerSprite.y = app.screen.height / 10;
hammerSprite.interactive = true;
hammerSprite.buttonMode = true;
hammerSprite.on('pointerdown', () => {
  gsap.to(hammerSprite, { alpha: 0, y: app.screen.height / 8 });
  gsap.to(menuContainer, { alpha: 1, y: app.screen.height / 2, onComplete: setInteractive });
});
gsap.to(hammerSprite, { y: app.screen.height / 10 + 20, duration: 2, yoyo: 1, repeat: -1})

export const finalSprite = new PIXI.Sprite(PIXI.Texture.from(final));
interfaceContainer.addChild(finalSprite);
finalSprite.anchor.set(.5);
finalSprite.x = 0;
finalSprite.y = 100;
finalSprite.alpha = 0;
finalSprite.scale.set(1.5)
