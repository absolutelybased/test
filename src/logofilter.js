import * as PIXI from 'pixi.js';

let totalTime = 0;
const fragment = `
varying vec2 vTextureCoord;
uniform sampler2D uSampler;
uniform float time;

void main()
{
  vec2 uv = vec2(vTextureCoord.x, 1.-vTextureCoord.y);
  vec2 vTextureCoord2 = vTextureCoord;
  vTextureCoord2.y += sin(time+uv.x)/10.;
  vTextureCoord2 = fract(vTextureCoord2);
  vec3 h = texture2D(uSampler, vTextureCoord2).rgb;
  h.r *= sin(time*10.);
  h.g *= sin(time*5.);
  h.b *= cos(time*2.);
  float z = 0.;
  if (h.r > .05) z = 1.; 

  gl_FragColor = vec4(h, z);
}
`;

export const logofilter = new PIXI.Filter(null, fragment, {
  time: 0.0,
});

logofilter.update = (delta) => {
  logofilter.uniforms.time = totalTime;
  totalTime += delta / 60;
}
