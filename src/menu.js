import * as PIXI from 'pixi.js';
import { gsap } from 'gsap';
import { finalSprite } from './interface';
import { oldStairSprite, redStairSprite, greenStairSprite, blueStairSprite } from './decorations';
import { app } from './setup';

import menuOpt from './assets/1.png';
import menuChoosed from './assets/choosed.png';
import menuOk from './assets/ok.png';
import carpetBlue from './assets/01.png';
import carpetRed from './assets/02.png';
import carpetGreen from './assets/03.png';

const menuOptTexture = PIXI.Texture.from(menuOpt);
const menuOptChTexture = PIXI.Texture.from(menuChoosed);

export const menuContainer = new PIXI.Container();
menuContainer.alpha = 0;
menuContainer.x = app.screen.width / 2 + 50;
menuContainer.y = app.screen.height / 2 + 50;


const menuSprite1 = new PIXI.Sprite(menuOptTexture);
menuContainer.addChild(menuSprite1);
menuSprite1.anchor.set(.5);
menuSprite1.x = 0;
menuSprite1.y = -app.screen.height / 10 + 30;
menuSprite1.scale.set(.75, .75);

const menuSprite2 = new PIXI.Sprite(menuOptTexture);
menuContainer.addChild(menuSprite2);
menuSprite2.anchor.set(.5);
menuSprite2.x = 100;
menuSprite2.y = -app.screen.height / 10;
menuSprite2.scale.set(.75, .75);

const menuSprite3 = new PIXI.Sprite(menuOptTexture);
menuContainer.addChild(menuSprite3);
menuSprite3.anchor.set(.5);
menuSprite3.x = 200;
menuSprite3.y = -app.screen.height / 10 - 30;
menuSprite3.scale.set(.75, .75);


const genMenuChSprite = () => {
  const menuChSprite = new PIXI.Sprite(menuOptChTexture);
  menuContainer.addChild(menuChSprite);
  menuChSprite.anchor.set(.5);
  menuChSprite.scale.set(.75, .75);
  menuChSprite.alpha = 0;
  menuChSprite.interactive = false;
  menuChSprite.buttonMode = true;
  return menuChSprite;
}

const menuChSprite1 = genMenuChSprite();
menuChSprite1.x = 0;
menuChSprite1.y = -app.screen.height / 10 + 30;
menuChSprite1.on('pointerdown', () => {
  gsap.to(menuChSprite1, { alpha: 1 });
  gsap.to(menuChSprite2, { alpha: 0 });
  gsap.to(menuChSprite3, { alpha: 0 });
  gsap.fromTo(okSprite,
    { alpha: 0, x: 0, y: -app.screen.height / 10 + 100 },
    {
      alpha: 1, x: 0, y: -app.screen.height / 10 + 80,
      onComplete: () => { okSprite.interactive = true; }
    });
  gsap.to(oldStairSprite, { alpha: 0, y: app.screen.height / 5 + 50, duration: .5 });
  gsap.fromTo(blueStairSprite, { alpha: 0, y: app.screen.height / 7 - 50 }, { duration: .5, alpha: 1, y: app.screen.height / 7 });
  gsap.to(redStairSprite, { duration: .5, alpha: 0, y: app.screen.height / 7 + 50 });
  gsap.to(greenStairSprite, { duration: .5, alpha: 0, y: app.screen.height / 7 + 50 });
});

const menuChSprite2 = genMenuChSprite();
menuChSprite2.x = 100;
menuChSprite2.y = -app.screen.height / 10;
menuChSprite2.on('pointerdown', () => {
  gsap.to(menuChSprite2, { alpha: 1 });
  gsap.to(menuChSprite1, { alpha: 0 });
  gsap.to(menuChSprite3, { alpha: 0 });
  gsap.fromTo(okSprite,
    { alpha: 0, x: 100, y: -app.screen.height / 10 + 70 },
    {
      alpha: 1, x: 100, y: -app.screen.height / 10 + 50,
      onComplete: () => { okSprite.interactive = true; }
    });
  gsap.to(oldStairSprite, { alpha: 0, y: app.screen.height / 5 + 50, duration: .5 });
  gsap.fromTo(redStairSprite, { alpha: 0, y: app.screen.height / 7 - 50 }, { duration: .5, alpha: 1, y: app.screen.height / 7 });
  gsap.to(blueStairSprite, { duration: .5, alpha: 0, y: app.screen.height / 7 + 50 });
  gsap.to(greenStairSprite, { duration: .5, alpha: 0, y: app.screen.height / 7 + 50 });
});

const menuChSprite3 = genMenuChSprite();
menuChSprite3.x = 200;
menuChSprite3.y = -app.screen.height / 10 - 30;
menuChSprite3.on('pointerdown', () => {
  gsap.to(menuChSprite3, { alpha: 1 });
  gsap.to(menuChSprite1, { alpha: 0 });
  gsap.to(menuChSprite2, { alpha: 0 });
  gsap.fromTo(okSprite,
    { alpha: 0, x: 200, y: -app.screen.height / 10 + 40 },
    {
      alpha: 1, x: 200, y: -app.screen.height / 10 + 20,
      onComplete: () => { okSprite.interactive = true; }
    });
  gsap.to(oldStairSprite, { alpha: 0, y: app.screen.height / 5 + 50, duration: .5 });
  gsap.fromTo(greenStairSprite, { alpha: 0, y: app.screen.height / 7 - 50 }, { duration: .5, alpha: 1, y: app.screen.height / 7 });
  gsap.to(blueStairSprite, { duration: .5, alpha: 0, y: app.screen.height / 7 + 50 });
  gsap.to(redStairSprite, { duration: .5, alpha: 0, y: app.screen.height / 7 + 50 });
});

export const setInteractive = () => {
  menuChSprite1.interactive = true;
  menuChSprite2.interactive = true;
  menuChSprite3.interactive = true;
}


const menuCarpetSprite1 = new PIXI.Sprite(PIXI.Texture.from(carpetBlue));
menuContainer.addChild(menuCarpetSprite1);
menuCarpetSprite1.anchor.set(0.25, .675);
menuCarpetSprite1.x = 0;
menuCarpetSprite1.y = -app.screen.height / 10 + 30;

const menuCarpetSprite2 = new PIXI.Sprite(PIXI.Texture.from(carpetRed));
menuContainer.addChild(menuCarpetSprite2);
menuCarpetSprite2.anchor.set(0.2, .775);
menuCarpetSprite2.x = 100;
menuCarpetSprite2.y = -app.screen.height / 10;

const menuCarpetSprite3 = new PIXI.Sprite(PIXI.Texture.from(carpetGreen));
menuContainer.addChild(menuCarpetSprite3);
menuCarpetSprite3.anchor.set(0.325, .6);
menuCarpetSprite3.x = 200;
menuCarpetSprite3.y = -app.screen.height / 10 - 30;

menuCarpetSprite1.scale.set(.75, .75);
menuCarpetSprite2.scale.set(.75, .75);
menuCarpetSprite3.scale.set(.75, .75);


const okSprite = new PIXI.Sprite(PIXI.Texture.from(menuOk));
menuContainer.addChild(okSprite);
okSprite.anchor.set(.5);
okSprite.scale.set(.75, .75);
okSprite.alpha = 0;
okSprite.interactive = false;
okSprite.buttonMode = true;
okSprite.on('pointerdown', () => {
  gsap.to(finalSprite, { y: 0, alpha: 1 });
})
