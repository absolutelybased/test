import * as PIXI from 'pixi.js';
import { gsap } from 'gsap';

// https://pixijs.io/guides/basics/graphics.html
// https://playable.playrix.com/demo/HS/

export const app = new PIXI.Application({
  width: window.innerWidth,
  height: window.innerHeight,
  antialias: true,
  // transparent: true,
  resolution: window.devicePixelRatio || 1,
  backgroundColor: 0,
  // forceCanvas: true,
});

document.body.appendChild(app.view);
app.renderer.autoResize = true;

app.ticker.stop();
gsap.ticker.add(() => {
    app.ticker.update();
});

gsap.defaults({
  duration: .25,
  ease: 'power4.out'
});
